package com.mygdx.game;

/**
 * Created by Blutbad on 04.09.2017.
 */

public class Constants {
    public static final String TITLE = "Flocking Simulator 0.1";
    public static final int APP_WIDTH = 1280;
    public static final int APP_HEIGHT = 720;

    public static final int MAX_SEE_DIST = 50;
    public static final int MAX_VIEW_ANGLE = 180;


    public static final int MINIMAL_SEPARATION = 25;
    public static final int DESIRED_SEPARATION = 50;



}
