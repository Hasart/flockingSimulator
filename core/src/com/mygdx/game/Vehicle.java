package com.mygdx.game;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Blutbad on 25.01.2018.
 */

public class Vehicle {
    Vector2 position, velocity, acceleration;
    boolean alive = true;
    float r, maxspeed, maxforce;
    Random rand;
    private ShapeRenderer shapeRenderer;

    public Vehicle(float x, float y, ShapeRenderer shapeRenderer) {
        rand = new Random();
        this.shapeRenderer = shapeRenderer;
        acceleration = new Vector2(0, 0);
        position = new Vector2(x, y);
        r = 6;
        maxspeed = 3;
        maxforce = 0.05f;
        velocity = new Vector2(rand.nextInt(8) - 4, rand.nextInt(8) - 4);
    }

    public void run(ArrayList<Vehicle> vehicles) {
        ArrayList<Vehicle> visibleVehicles = getVisibleVehicles(vehicles);

        Vector2 sep = separate(visibleVehicles);   // Separation
        Vector2 ali = align(visibleVehicles);      // Alignment
        Vector2 coh = cohesion(visibleVehicles);   // Cohesion
        Vector2 arrive = arrive(new Vector2(Constants.APP_WIDTH / 2, Constants.APP_HEIGHT / 2));

        // Arbitrarily weight these forces
        sep.scl(2f);
        ali.scl(1.0f);
        coh.scl(1.0f);
        arrive.scl(2.0f);

        // Add the force vectors to acceleration
        applyForce(sep);
        applyForce(ali);
        applyForce(coh);
//        applyForce(arrive);

        update();
        display();
    }

    // Method to update position
    void update() {
        if (!alive) {
            velocity = new Vector2(0, 0);
            position = new Vector2(0, 0);
        }

        velocity.add(acceleration);
        velocity.limit(maxspeed);
        position.add(velocity);
        acceleration.setZero();

        if (position.x < -r) position.x = Constants.APP_WIDTH + r;
        if (position.y < -r) position.y = Constants.APP_HEIGHT + r;
        if (position.x > Constants.APP_WIDTH + r) position.x = -r;
        if (position.y > Constants.APP_HEIGHT + r) position.y = -r;
    }

    void applyForce(Vector2 force) {
        acceleration.add(force);
    }

    // A method that calculates a steering force towards a target
    // STEER = DESIRED MINUS VELOCITY
    Vector2 seek(Vector2 target) {
        Vector2 desired = new Vector2(target.x - position.x, target.y - position.y);  // A vector pointing from the position to the target

        // Scale to maximum speed
        desired.nor();
        desired.scl(maxspeed);

        // Steering = Desired minus velocity
        Vector2 steer = new Vector2(desired.x - velocity.x, desired.y - velocity.y);
        steer.limit(maxforce);  // Limit to maximum steering force
        return (steer);
    }

    Vector2 arrive(Vector2 target) {
        Vector2 desired = target.sub(position);  // A vector pointing from the position to the target
        float d = desired.len();
        if (d < 50) {
            float m = (maxspeed / 50) * d;
            desired.setLength2(m);
        } else {
            desired.setLength2(maxspeed);
        }

        // Steering = Desired minus Velocity
        Vector2 steer = desired.sub(velocity);
        steer.limit(maxforce);  // Limit to maximum steering force
        return steer;
    }

    ArrayList<Vehicle> getVisibleVehicles(ArrayList<Vehicle> vehicles) {
        ArrayList<Vehicle> result = new ArrayList<Vehicle>();
        for (Vehicle vehicle : vehicles) {
            if (alive && vehicle.alive && this.isSeeing(vehicle)) {  // pokud je v zornem poli
                result.add(vehicle);
            }
        }
        return result;
    }

    boolean isSeeing(Vehicle vehicle) {
        if (position.dst(vehicle.position) < Constants.MAX_SEE_DIST && Math.abs(this.velocity.angle(vehicle.position)) < Constants.MAX_VIEW_ANGLE) { // je ve spravnem uhlu
            return true;
        }
        return false;
    }

    void display() {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(0, 1, 0, 1);
        shapeRenderer.circle(position.x, position.y, r);
        Vector2 direction = velocity.cpy();
        direction.scl(500);
        direction.limit(20);
        shapeRenderer.line(position, direction.add(position));
        shapeRenderer.end();
    }

    private Vector2 separate(ArrayList<Vehicle> vehicles) {
        Vector2 steer = new Vector2(0, 0);
        int count = 0;
        // For every boid in the system, check if it's too close
        for (Vehicle other : vehicles) {
            float d = Vector2.dst(position.x, position.y, other.position.x, other.position.y);
            if (d < Constants.MINIMAL_SEPARATION && d != 0) {
                // Calculate vector pointing away from neighbor
                Vector2 diff = position.cpy().sub(other.position);
                diff.nor();
                diff.scl(1 / d);        // Weight by distance
                steer.add(diff);
                count++;            // Keep track of how many
            }
        }
        // Average -- divide by how many
        if (count > 0) {
            steer.scl((float) 1 / count);
        }
        // As long as the vector is greater than 0
        if (steer.len() > 0) {
            // Implement Reynolds: Steering = Desired - Velocity
            steer.nor();
            steer.scl(maxspeed);
            steer.sub(velocity);
            steer.limit(maxforce);
        }
        return steer;
    }

    private Vector2 align(ArrayList<Vehicle> visibleVehicles) {
        Vector2 sum = new Vector2(0, 0);
        int count = 0;
        for (Vehicle other : visibleVehicles) {
            sum.add(other.velocity);
            count++;
        }
        if (count > 0) {
            sum.x = sum.x / count;
            sum.y = sum.y / count;

            sum.nor();
            sum.scl(maxspeed);
            Vector2 steer = sum.cpy().sub(velocity);
            steer.limit(maxforce);
            return steer;
        } else {
            return new Vector2(0, 0);
        }
    }

    // Cohesion
    // For the average position (i.e. center) of all nearby boids, calculate steering vector towards that position
    private Vector2 cohesion(ArrayList<Vehicle> visibleVehicles) {
        Vector2 sum = new Vector2(0, 0);   // Start with empty vector to accumulate all positions
        for (Vehicle other : visibleVehicles) {
            sum.add(other.position); // Add position
        }
        if (visibleVehicles.size() > 0) {
            sum.x = sum.x / visibleVehicles.size();
            sum.y = sum.y / visibleVehicles.size();
            return seek(sum);  // Steer towards the position
        } else {
            return new Vector2(0, 0);
        }
    }
}
