package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;

/**
 * Created by Blutbad on 26.01.2018.
 */

public class ControlPanel extends ApplicationAdapter {
    public ControlPanel() {
        super();
    }

    @Override
    public void create() {
        super.create();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void pause() {
        super.pause();
    }

    @Override
    public void resume() {
        super.resume();
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
