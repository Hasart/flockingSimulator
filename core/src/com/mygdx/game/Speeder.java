package com.mygdx.game;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Blutbad on 29.01.2018.
 */

public class Speeder extends Vehicle {

    private final float x;
    private final float y;
    private final ShapeRenderer shapeRenderer;

    public Speeder(float x, float y, ShapeRenderer shapeRenderer) {
        super(x, y, shapeRenderer);
        this.x = x;
        this.y = y;
        this.shapeRenderer = shapeRenderer;
        maxspeed = 10;
    }
    void display() {
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(1, 0, 0, 1);
        shapeRenderer.circle(position.x, position.y, r);

        Vector2 direction = velocity.cpy();
        direction.scl(500);
        direction.limit(20);

        shapeRenderer.line(position, direction.add(position));
        shapeRenderer.end();


    }

}
