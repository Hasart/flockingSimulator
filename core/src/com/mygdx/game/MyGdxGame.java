package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.ArrayList;
import java.util.Random;

public class MyGdxGame extends ApplicationAdapter {
    public ShapeRenderer shapeRenderer;
    Random rand;
    ArrayList<Vehicle> vehicles;
    boolean controlPanelBool = true;

    @Override
    public void create() {
        shapeRenderer = new ShapeRenderer();
        vehicles = new ArrayList<Vehicle>();
        rand = new Random();

        if (controlPanelBool) {
            ControlPanel controlPanel = new ControlPanel();
        }

        for (int i = 0; i < 1000; i++) {
            vehicles.add(new Vehicle(rand.nextInt(Constants.APP_WIDTH), rand.nextInt(Constants.APP_HEIGHT), shapeRenderer));
        }
        vehicles.add(new Speeder(rand.nextInt(Constants.APP_WIDTH), rand.nextInt(Constants.APP_HEIGHT), shapeRenderer));
    }

    @Override
    public void render() {
        Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

//        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
//        shapeRenderer.setColor(0f, 1f, 0f, 1);
//        shapeRenderer.end();

        for (Vehicle v : vehicles) {
            ArrayList<Vehicle> clone = (ArrayList<Vehicle>) vehicles.clone();
            clone.remove(v);
            v.run(clone);
        }
    }

    @Override
    public void dispose() {
        shapeRenderer.dispose();
    }
}
